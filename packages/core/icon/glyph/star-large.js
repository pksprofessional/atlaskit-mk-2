'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StarLargeIcon = function StarLargeIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path fill="currentColor" d="M15.565 21.614L12 19.663l-3.564 1.95C6.45 22.7 4.71 21.38 5.089 19.084l.68-4.133-2.883-2.927c-1.606-1.63-.938-3.76 1.278-4.096l3.985-.603 1.783-3.76c.995-2.1 3.152-2.077 4.145.019l1.774 3.74 3.985.604c2.22.336 2.882 2.468 1.278 4.096l-2.883 2.927.68 4.132c.38 2.302-1.364 3.617-3.346 2.532zm.366-7.39l3.862-4c.114-.118.068-.236-.095-.26l-5.31-.82-2.26-4.867c-.07-.153-.187-.15-.256 0l-2.26 4.866-5.31.82c-.167.026-.21.143-.095.262l3.862 4-.885 5.48c-.026.163.073.228.214.149L12 17.283l4.602 2.57c.144.082.24.016.214-.147l-.885-5.482z"/></svg>' }, props));
};
exports.default = StarLargeIcon;